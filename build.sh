#!/bin/bash -e

PHP_MINOR_VERSION=1

echo "Building layer for PHP 8.$PHP_MINOR_VERSION - using Remi repository"

yum install -y yum-utils ca-certificates
yum update -y
yum install autoconf bison gcc gcc-c++ libcurl-devel libxml2-devel re2c openssl-devel -y

# curl -sL http://www.openssl.org/source/openssl-1.0.1k.tar.gz | tar -xvz
# cd openssl-1.0.1k
# ./config && make && make install
# cd ..

mkdir -p /tmp/environment/sql
cd /tmp/environment/sql
curl https://www.sqlite.org/2023/sqlite-autoconf-3420000.tar.gz | tar xzf -
cd ./sqlite-autoconf-3420000 && ./configure
make && make install

mkdir -p /tmp/environment/php-8-bin
curl -sL https://github.com/php/php-src/archive/php-8.1.19.tar.gz | tar -xvz
cd php-src-php-8.1.19

# Compile PHP 7.3.0 with OpenSSL 1.0.1 support, and install to /home/ec2-user/php-7-bin
./buildconf --force
./configure --help
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/lib"
export LD_RUN_PATH="$LD_RUN_PATH:/usr/local/lib"
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig"
./configure --prefix=$(realpath /tmp/environment/php-8-bin/) --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-pcntl --with-gd --with-json --with-intl --with-mbstring
make install



cd /tmp/environment/php-8-bin
ls -la

cp /opt/layer/bootstrap bootstrap
sed "s/PHP_MINOR_VERSION/${PHP_MINOR_VERSION}/g" /opt/layer/php.ini >php.ini

chmod +x bootstrap

zip -r /opt/layer/php8${PHP_MINOR_VERSION}.zip .