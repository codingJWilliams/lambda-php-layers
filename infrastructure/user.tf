resource "aws_iam_user" "gitlab" {
  name = module.this.id
  path = "/"
}

resource "aws_iam_user_policy" "gitlab" {
  name = module.this.id
  user = aws_iam_user.gitlab.name

  policy = data.aws_iam_policy_document.gitlab.json
}

data "aws_iam_policy_document" "gitlab" {
  statement {
    actions   = ["ecr-public:*"]
    resources = ["*"]
    effect = "Allow"
  }
  statement {
    actions   = ["lambda:ListLayers", "lambda:AddLayerVersionPermission", "lambda:GetLayerVersion", "lambda:ListLayers", "lambda:PublishLayerVersion", "sts:GetServiceBearerToken"]
    resources = ["*"]
    effect = "Allow"
  }
    statement {
    actions   = ["s3:PutObject", "s3:GetObject", "s3:GetObjectVersion"]
    resources = ["${aws_s3_bucket.storage.arn}/*"]
    effect = "Allow"
  }
}

resource "aws_iam_access_key" "gitlab" {
  user = aws_iam_user.gitlab.name
}

output "AWS_ACCESS_KEY_ID" {
  value = aws_iam_access_key.gitlab.id
}

output "AWS_SECRET_ACCESS_KEY" {
  value     = aws_iam_access_key.gitlab.secret
  sensitive = true
}

output "AWS_DEFAULT_REGION" {
  value = var.region
}

output "AWS_ECR_USERNAME" {
  value = "You need to find this from the AWS console"
}

output "AWS_BUCKET_NAME_PREFIX" {
  value = "${module.this.id}-${var.region}"
}

output "LAYER_NAME" {
    value="PHPLambda"
}
